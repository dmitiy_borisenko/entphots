#include "pycanvas.h"
#include <TBufferJSON.h>
#include <TGraph.h>
#include <TProcessID.h>

const char* HTML =
"   <div id=\"pycanvas_%d\" style=\"width: %dpx; height: %dpx\"></div>  "
"   <script>                                                            "
"       require(['scripts/JSRootCore'], function(jsroot) {              "
"           var canvas = jsroot.JSONR_unref(%s);                        "
"           jsroot.draw(\"pycanvas_%d\", canvas);                       "
"       });                                                             "
"   </script>;                                                          ";


PyCanvas::PyCanvas()
{
    TProcessID::AssignID(this);
}

PyCanvas::PyCanvas(int w, int h)
: TCanvas("", "", w, h)
{
    TProcessID::AssignID(this);
}

TString PyCanvas::getHtml() const 
{ 
    UInt_t uid = GetUniqueID();
    TString json = TBufferJSON::ConvertToJSON(this).ReplaceAll("PyCanvas", "TCanvas");
    return TString::Format(HTML, uid, GetWw(), GetWh(), json.Data(), uid);
}