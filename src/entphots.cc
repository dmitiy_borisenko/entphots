#include "entphots.h"
#include <experimental/array>
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>
#include <TLeaf.h>
#include <TPython.h>
#include <TObjString.h>
#include <algorithm>

using namespace std;

const int MAX_INT = numeric_limits<int>::max();

bool checkFilter(Consts::EventFilter f1, Consts::EventFilter f2) { return (f1|f2) == f1; }



ChanMapping<Consts::EAST_MAP.size()> Consts::EAST_MAP = experimental::to_array(Consts::EAST_MAP_CONTENT);
ChanMapping<Consts::WEST_MAP.size()> Consts::WEST_MAP = experimental::to_array(Consts::WEST_MAP_CONTENT);



Subrange::Subrange() {}

Subrange::Subrange(int start, int finish)
: beg(start)
, end(finish)
, ext(numeric_limits<Short_t>::max())
, type(Consts::CMN)
{}

Consts::SubrangeType Subrange::getType() const { return (Consts::SubrangeType)type; }

bool Subrange::operator<(const Subrange &other) { return dev < other.dev; }



Frame::Frame(bool doInit)
: size(0)
, orig( doInit ? new Short_t[Consts::MAX_NSAMPLES] : nullptr )
, inv( doInit ? new Int_t[Consts::MAX_NSAMPLES] : nullptr )
{}

Frame::~Frame()
{
    delete [] orig;
    delete [] inv;
}

void Frame::fillInv()
{
    subs.clear();
    bline = 0;
    if (size == 0) return;

    for (int i=0; i < Consts::NSAMPLES_FOR_BLINE/Consts::SUBRANGE_LEN; ++i)
    {
        Subrange sub(i*Consts::SUBRANGE_LEN, (i + 1)*Consts::SUBRANGE_LEN);
        if (sub.end > size) break;
        
        double mean = 0;
        for (int s=sub.beg; s < sub.end; ++s) 
        {
            mean += orig[s];
            if (orig[s] < sub.ext) sub.ext = orig[s];
        }
        sub.mean = (int)round(mean/Consts::SUBRANGE_LEN);

        double dev = 0;
        for (int s=sub.beg; s < sub.end; ++s) dev += abs(sub.mean - orig[s]);
        sub.dev = (int)round(dev/Consts::SUBRANGE_LEN);

        subs.push_back(sub);
    }

    // Первоначальная оценка bline
    vector<Subrange>::iterator ref_it = min_element(subs.begin(), subs.end());
    ref_it->type = Consts::REF;
    bline = ref_it->mean;

    // Нужна ли коррекция?
    vector<Subrange>::iterator viol_it = find_if(
        subs.begin(), 
        ref_it, 
        [=](const Subrange &sub) { return bline - sub.ext > Consts::THRES; }
    );
    if (viol_it != ref_it)
    {
        ref_it->type = Consts::REF_RAW;
        // Если возможно, то делаем коррекцию
        if (viol_it != subs.begin())
        {
            ref_it = min_element(subs.begin(), viol_it);
            ref_it->type = Consts::REF;
            bline = ref_it->mean;
        }
    }

    for (int s=0; s < size; ++s) inv[s] = bline - orig[s];
    for (auto &sub : subs) sub.ext = bline - sub.ext;
}



FrameView::FrameView()
: can(450, 300)
, inv(1)
, ref(2)
, ref_raw(2)
{
    inv.SetEditable(kFALSE);
    inv.Draw();
    
    ref.SetEditable(kFALSE);
    ref.SetLineColor(kGreen);
    ref.SetLineWidth(3);
    ref.Draw("same");

    ref_raw.SetEditable(kFALSE);
    ref_raw.SetLineColor(kRed);
    ref_raw.SetLineWidth(3);
    ref_raw.Draw("same");
}

void FrameView::setN(int n) { inv.Set(n); }




Channel::Channel(TBranch* branch)
: br(branch)
, fr(nullptr)
{
    if (br) view.inv.SetTitle(br->GetName());
}

Channel::~Channel()
{
    delete fr;
}

TString Channel::getHtml()
{
    TString html;
    if (!fr || !fr->size) return html;

    // Подробности определения bline
    html = "<table border=\"1\"><tr><th>#</th><th></th><th>Сред</th><th>Откл</th><th>Макс</th></tr>";
    int i=1;
    for (auto &sub : fr->subs)
    {
        const char* marker;
        switch (sub.type)
        {
            case Consts::CMN:       marker = "black"; break;
            case Consts::REF:       marker = "green"; break;
            case Consts::REF_RAW:   marker = "red";
        }

        html += TString::Format(
            "<tr style=\"color: %s\"><td>%d</td><td>[%d,%d)</td><td>%d</td><td>%d</td><td>%d</td></tr>", 
            marker,
            i++, 
            sub.beg, 
            sub.end,
            sub.mean,
            sub.dev,
            sub.ext);
    }
    html += "</table>";

    // html-представление канвы
    html += view.can.getHtml();

    return html;
}

Channel* Channel::getForWriting(TBranch* branch)
{
    Channel* ch = new Channel(branch);
    if (ch->br == nullptr) return ch;
    ch->fr = new Frame(true);
    ch->br->GetLeaf("wf_size")->SetAddress(&ch->fr->size);
    ch->br->GetLeaf("wf")->SetAddress(ch->fr->orig);
    return ch;
}

Channel* Channel::getForReading(TBranch* branch)
{
    Channel* ch = new Channel(branch);
    if (ch->br == nullptr) return ch;
    ch->fr = new Frame();
    ch->br->SetAddress(&ch->fr);
    return ch;
}




template<ChanAction ACT>
ArmProcessor<ACT>::ArmProcessor() {}

template<ChanAction ACT>
int ArmProcessor<ACT>::doEventAct(Long64_t ev)
{
    int n_det_hits = 0, diff_fac = -1;
    
    for (auto ch : chans)
    {
        if (ch->br && ACT(ch, ev)) ++n_det_hits;
    }
    
    if (chans[0]->fr && chans[0]->fr->size) 
    {
        --n_det_hits;
        diff_fac = 1;
    }
    if (n_det_hits == 0) n_det_hits = MAX_INT;
    
    return n_det_hits * diff_fac;
}

template<ChanAction ACT>
ArmProcessor<ACT>::~ArmProcessor()
{
    for (auto ch : chans) delete ch;
}




bool convAction(Channel* ch, Long64_t ev)
{
    ch->br->GetEntry(ev);
    ch->fr->fillInv();
    return ch->fr->size;
}

template<int N, ChanMapping<N> &MAP>
ArmConverter<N, MAP>::ArmConverter(TTree* in_tree)
: tree(in_tree)
{
    for (auto br_name : MAP) chans.push_back( Channel::getForWriting(tree->GetBranch(br_name)) );
}

template<int N, ChanMapping<N> &MAP>
void ArmConverter<N, MAP>::preConvert(TTree* out_tree)
{
    for (auto ch : chans)
    {
        if (ch->br) out_tree->Branch(ch->br->GetName(), ch->fr);
    }
}




Converter::Converter(const char* in_fname)
: tree( (TTree*) TFile::Open(in_fname)->Get("adc64_data") )
, eastArm(tree)
, westArm(tree)
{}

void Converter::convert(const char* out_fname)
{
    TFile f(out_fname, "recreate");
    TTree t("adc64_data", "");
    eastArm.preConvert(&t);
    westArm.preConvert(&t);

    for (Long64_t ev=0; ev < tree->GetEntries(); ++ev)
    {
        if ((ev + 1)%10000 == 0) printf("Ev: %lld\n", ev + 1);
        eastArm.doEventAct(ev);
        westArm.doEventAct(ev);
        t.Fill();
    }

    t.Write();
}




bool readAction(Channel* ch, Long64_t ev)
{
    ch->br->GetEntry(ev);

    ch->view.setN(ch->fr->size);
    for (int s=0; s < ch->fr->size; ++s)
    {
        ch->view.inv.SetPoint(s, s, ch->fr->inv[s]);
    }

    double *xs, *ys;

    vector<Subrange>::iterator it = find_if(
        ch->fr->subs.begin(),
        ch->fr->subs.end(),
        [=](const Subrange &sub) { return sub.getType() == Consts::REF; }
    );
    xs = ch->view.ref.GetX();
    if (it != ch->fr->subs.end())
    {
        xs[0] = it->beg;
        xs[1] = it->end - 1;
    }
    else xs[1] = xs[0];

    it = find_if(
        ch->fr->subs.begin(),
        ch->fr->subs.end(),
        [=](const Subrange &sub) { return sub.getType() == Consts::REF_RAW; }
    );
    xs = ch->view.ref_raw.GetX();
    ys = ch->view.ref_raw.GetY();
    if (it != ch->fr->subs.end())
    {
        xs[0] = it->beg;
        xs[1] = it->end - 1;
        ys[0] = ys[1] = ch->fr->bline - it->mean;
    }
    else xs[1] = xs[0];

    return ch->fr->size;
}

template<int N, ChanMapping<N> &MAP>
ArmReader<N, MAP>::ArmReader(TTree* in_tree)
: tree(in_tree)
, persEv(-1)
{
    for (auto br_name : MAP) chans.push_back( Channel::getForReading(in_tree->GetBranch(br_name)) );
}

template<int N, ChanMapping<N> &MAP>
PyObject* ArmReader<N, MAP>::getHTML(const char* div_id)
{
    TObjString armContent;
    armContent.String().Form("<div id=\"%s\">", div_id);
    for (auto ch : chans) armContent.String() += ch->getHtml(); 
    armContent.String() += "</div>";

    TPython::Bind(&armContent, "arm_content");
    return TPython::Eval("HTML(data=arm_content.GetName())");
}

template<int N, ChanMapping<N> &MAP>
bool ArmReader<N, MAP>::read(Long64_t ev, Consts::EventFilter filter)
{
    if (ev != persEv) 
    {
        persVal = doEventAct(ev);
        persEv = ev;
    }
    
    if (checkFilter(filter, Consts::DIFF) && persVal < 0) return true;
    return checkFilter(filter, Consts::ALO_DET) && abs(persVal) == MAX_INT;
}





bool Reader::dummy = Reader::init();

bool Reader::init()
{
    TPython::Exec("from IPython.display import HTML");
    return true;
}

Reader::Reader(const char* fname)
: tree( (TTree*) TFile::Open(fname)->Get("adc64_data") )
, eastArm(tree)
, westArm(tree)
, curEv(0)
{
    read(Consts::ANY);
}

bool Reader::read(Consts::EventFilter e_filter, Consts::EventFilter w_filter)
{
    return eastArm.read(curEv, e_filter) || westArm.read(curEv, w_filter);
}

bool Reader::read(Consts::EventFilter cmb_filter)
{
    Consts::EventFilter sym_filter = Consts::EventFilter(cmb_filter >> 2);
    Consts::EventFilter asym_filter = Consts::EventFilter(cmb_filter & 0x3);

    if (read(sym_filter, sym_filter)) return true;
    return eastArm.read(curEv, asym_filter) && westArm.read(curEv, asym_filter);
}

void Reader::stepBack()
{
    --curEv;
    if (curEv < 0) curEv += tree->GetEntries();
}

void Reader::stepForth()
{
    ++curEv;
    if (curEv >= tree->GetEntries()) curEv -= tree->GetEntries();
}

void Reader::prevEvent(Consts::EventFilter e_filter, Consts::EventFilter w_filter)
{
    do stepBack(); while (read(e_filter, w_filter));
}

void Reader::nextEvent(Consts::EventFilter e_filter, Consts::EventFilter w_filter)
{
    do stepForth(); while (read(e_filter, w_filter));
}

void Reader::prevEvent(Consts::EventFilter cmb_filter)
{
    do stepBack(); while (read(cmb_filter));
}

void Reader::nextEvent(Consts::EventFilter cmb_filter)
{
    do stepForth(); while (read(cmb_filter));
}

PyObject* Reader::getEastHTML() { return eastArm.getHTML("east"); }

PyObject* Reader::getWestHTML() { return westArm.getHTML("west"); }