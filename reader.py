import ROOT
from ipywidgets import Output, HBox, Button
from IPython.display import HTML, display

ROOT.gSystem.Load('entphots2/bld/libEntPhots.so')

reader = 0

head = Output()
east = Output()
west = Output()
box = HBox([west, east])

@head.capture()
def update_head():
    html = HTML(data='<h3>Ev: {}</h3>'.format(reader.curEv))
    display(html, display_id='event', update=bool(head.outputs))

@east.capture()
def update_east():
    display(reader.getEastHTML(), display_id='east', update=bool(east.outputs))
    
@west.capture()
def update_west():
    display(reader.getWestHTML(), display_id='west', update=bool(west.outputs))

def update():
    update_head()
    update_east()
    update_west()

def prevEv(_):
    reader.prevEvent()
    update()
    
def nextEv(_):
    reader.nextEvent()
    update()

prevBtn = Button(description='PREV')
prevBtn.on_click(prevEv)
nextBtn = Button(description='NEXT')
nextBtn.on_click(nextEv)

def set_root_file(fname):
    global reader
    reader = ROOT.Reader(fname)

def display_reader():
    head.outputs = ()
    east.outputs = ()
    west.outputs = ()
    display(prevBtn, nextBtn, head, box)
    update()