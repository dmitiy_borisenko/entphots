#ifdef ENTPHOTS_H
    #pragma link C++ struct Consts;
    #pragma link C++ struct Subrange+;
    #pragma link C++ struct Frame+;
    #pragma link C++ class Converter;
    #pragma link C++ class Reader;
#endif

#if defined(PYCANVAS_H) && !defined(ENTPHOTS_H)
    #pragma link C++ class PyCanvas;
#endif