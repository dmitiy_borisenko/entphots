# Dictionaries
ENTPHOTS_DICT := bld/entphots.cxx
PYCANVAS_DICT := bld/pycanvas.cxx

# Libraries 
LIB := bld/libEntPhots.so

# Applications
TEST := bld/test

CLING = rootcling -I$(realpath inc) -f $@ -c $(foreach header,$^,$(notdir $(header)))
COMPILER := g++ -g $(shell root-config --cflags --libs) -lROOTTPython -I./inc
LIB_COMPILER = $(COMPILER) -shared -fPIC -o $@ $^
EXE_COMPILER = $(COMPILER) -L./bld -lEntPhots -o $@ $<

.PHONY: clean 

$(TEST): test.cc $(LIB)
	$(EXE_COMPILER)

$(LIB): src/entphots.cc $(ENTPHOTS_DICT) src/pycanvas.cc $(PYCANVAS_DICT)
	$(LIB_COMPILER)

$(ENTPHOTS_DICT):: inc/entphots.h LinkDef.h
	$(CLING)

$(PYCANVAS_DICT):: inc/pycanvas.h LinkDef.h
	$(CLING)

clean:
	@find bld/* -exec rm -vf {} \;