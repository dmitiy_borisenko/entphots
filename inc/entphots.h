#ifndef ENTPHOTS_H
#define ENTPHOTS_H

#include <Rtypes.h>
#include <array>
#include <TGraph.h>
#include <vector>
// #include <TCanvas.h>
#include "pycanvas.h"
#include <TPyReturn.h>

template<int N>
using ChanMapping = const std::array<const char*, N>;

struct Consts
{
    Consts() = delete;

private:
    constexpr static const char* EAST_MAP_CONTENT[] = {
        "channel_32", 
        
        "channel_0",
        "channel_1",
        "channel_2",
        "channel_3",
        "channel_4",
        "channel_5",
        "channel_6",
        "channel_7",
        "channel_8",
        "channel_9",
        "channel_10",
        "channel_11",
        "channel_12",
        "channel_13",
        "channel_14",
        "channel_15",
    };
    constexpr static const char* WEST_MAP_CONTENT[] = {
        "channel_33", 
        
        "channel_16",
        "channel_17",
        "channel_18",
        "channel_19",
        "channel_20",
        "channel_21",
        "channel_22",
        "channel_23",
        "channel_24",
        "channel_25",
        "channel_26",
        "channel_27",
        "channel_28",
        "channel_29",
        "channel_30",
        "channel_31",
    };

public:
    enum SubrangeType {
        CMN,
        REF_RAW,
        REF
    };

    enum EventFilter { 
        ANY, 
        DIFF, 
        ALO_DET, 
        FULL        = DIFF | ALO_DET,

        CMB_HALF    = DIFF << 2,
        CMB_ALMOST  = CMB_HALF | ALO_DET,
        CMB_FULL    = FULL << 2
    };

    static const int MAX_NSAMPLES = 2048;
    static ChanMapping<sizeof(EAST_MAP_CONTENT)/sizeof(EAST_MAP_CONTENT[0])> EAST_MAP;
    static ChanMapping<sizeof(WEST_MAP_CONTENT)/sizeof(WEST_MAP_CONTENT[0])> WEST_MAP;
    static const int NSAMPLES_FOR_BLINE = 150;
    static const int SUBRANGE_LEN = 25;
    static const int THRES = 400;

ClassDef(Consts, 0)
};



struct Subrange 
{
    UShort_t    beg;
    UShort_t    end;
    Int_t       ext;
    Short_t     mean;
    UShort_t    dev;
    Byte_t      type;

    Subrange();
    Subrange(int start, int finish);
    Consts::SubrangeType getType() const;
    bool operator<(const Subrange &);

ClassDef(Subrange, 1)
};



struct Frame
{
    std::vector<Subrange>   subs;
    Short_t                 bline;
    Int_t                   size;
    Short_t*                orig;       //!
    Int_t*                  inv;        //[size]

    Frame(bool doInit=false);
    ~Frame();
    void fillInv();

ClassDef(Frame, 1)
};



struct FrameView 
{
    // TCanvas     can;
    PyCanvas    can;
    TGraph      inv;
    TGraph      ref;
    TGraph      ref_raw;

    FrameView();
    void setN(int n);
};



class TBranch;

struct Channel
{
    TBranch*    br;
    Frame*      fr;
    FrameView   view;

    Channel(TBranch*);
    ~Channel();
    TString getHtml();

    static Channel* getForWriting(TBranch*);
    static Channel* getForReading(TBranch*);
};



typedef bool (&ChanAction)(Channel*, Long64_t);

template<ChanAction ACT>
class ArmProcessor
{
protected:
    ArmProcessor();

public:
    std::vector<Channel*> chans;

    ~ArmProcessor();
    int doEventAct(Long64_t);
};



bool convAction(Channel*, Long64_t);
class TTree;

template<int N, ChanMapping<N> &MAP>
class ArmConverter : public ArmProcessor<convAction>
{
    TTree*  tree;

public:
    ArmConverter(TTree* in_tree);
    void preConvert(TTree* out_tree);
};

using EastConverter = ArmConverter<Consts::EAST_MAP.size(), Consts::EAST_MAP>;
using WestConverter = ArmConverter<Consts::WEST_MAP.size(), Consts::WEST_MAP>;

class Converter 
{
    TTree*          tree;
    EastConverter   eastArm;
    WestConverter   westArm;

public:
    Converter(const char* in_fname);
    void convert(const char* out_fname);

ClassDef(Converter, 0)
};



bool readAction(Channel*, Long64_t);

template<int N, ChanMapping<N> &MAP>
class ArmReader : public ArmProcessor<readAction>
{
    TTree*      tree;
    Long64_t    persEv;
    int         persVal;

public:
    ArmReader(TTree*);
    PyObject* getHTML(const char* div_id="");
    bool read(Long64_t, Consts::EventFilter);
};

using EastReader = ArmReader<Consts::EAST_MAP.size(), Consts::EAST_MAP>;
using WestReader = ArmReader<Consts::WEST_MAP.size(), Consts::WEST_MAP>;

class Reader
{
    TTree*      tree;
    EastReader  eastArm;
    WestReader  westArm;

    static bool dummy;
    static bool init();

    bool read(Consts::EventFilter east, Consts::EventFilter west);
    bool read(Consts::EventFilter both);
    void stepBack();
    void stepForth();

public:
    Long64_t    curEv;

    Reader(const char* fname);
    void prevEvent(Consts::EventFilter east, Consts::EventFilter west);
    void nextEvent(Consts::EventFilter east, Consts::EventFilter west);
    void prevEvent(Consts::EventFilter both=Consts::CMB_ALMOST);
    void nextEvent(Consts::EventFilter both=Consts::CMB_ALMOST);
    PyObject* getEastHTML();
    PyObject* getWestHTML();

ClassDef(Reader, 0)
};

#endif