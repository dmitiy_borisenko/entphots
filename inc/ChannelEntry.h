#ifndef CHANNEL_ENTRY_H
#define CHANNEL_ENTRY_H

const int MAX_N_SAMPLES = 2048;

struct ChannelEntry {
    Int_t integral_in_gate;
    Short_t zero_level;
    Short_t peak_pos;
    Short_t amp;
    Short_t wf_size;
    Short_t wf[MAX_N_SAMPLES];

};

#endif