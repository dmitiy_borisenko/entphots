#ifndef PYCANVAS_H
#define PYCANVAS_H

#include <TCanvas.h>

class PyCanvas : public TCanvas
{
    TString htmlContent;
 
public:
    PyCanvas();
    PyCanvas(int w, int h);
    TString getHtml() const;

ClassDef(PyCanvas, 0)
};


#endif